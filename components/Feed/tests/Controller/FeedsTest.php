<?php

declare(strict_types = 1);

// {{{ License

// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.

// }}}

namespace Component\Feed\tests\Controller;

use App\Core\Router;
use App\Util\GNUsocialTestCase;
use Component\Feed\Controller\Feeds;
use Jchook\AssertThrows\AssertThrows;

class FeedsTest extends GNUsocialTestCase
{
    use AssertThrows;

    public function testPublic()
    {
        // This calls static::bootKernel(), and creates a "client" that is acting as the browser
        $client  = static::createClient();
        $crawler = $client->request('GET', Router::url('feed_public'));
        $this->assertResponseIsSuccessful();
    }

    public function testHome()
    {
        // This calls static::bootKernel(), and creates a "client" that is acting as the browser
        $client  = static::createClient();
        $crawler = $client->request('GET', Router::url('feed_home'));
        $this->assertResponseStatusCodeSame(302);
    }

    // TODO: It would be nice to actually test whether the feeds are respecting scopes and spitting
    // out the expected notes... The ActivityPub plugin have a somewhat obvious way of testing it so,
    // for now, having that, might fill that need, let's see
}
