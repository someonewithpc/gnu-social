<?php

declare(strict_types = 1);

// {{{ License
// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.
// }}}

namespace App\Tests\Entity;

use App\Core\DB;
use App\Core\VisibilityScope;
use App\Entity\Actor;
use App\Entity\Note;
use App\Util\GNUsocialTestCase;
use Functional as F;
use Jchook\AssertThrows\AssertThrows;

class NoteTest extends GNUsocialTestCase
{
    use AssertThrows;

    // public function testGetReplies()
    // {
    //     $user    = DB::findOneBy('local_user', ['nickname' => 'taken_user']);
    //     $notes = DB::findBy('note', ['actor_id' => $user->getId(), 'content' => 'some content', 'reply_to' => null]);
    //     dd($notes, F\map($notes, fn ($n) => $n->getReplies()), DB::dql('select n from note n'));
    //     $note    = DB::findOneBy('note', ['actor_id' => $user->getId(), 'content' => 'some content', 'reply_to' => null]);
    //     $replies = $note->getReplies();
    //     // dd($note, $replies);
    //     static::assertSame('some other content', $replies[0]->getContent());
    //     static::assertSame($user->getId(), $replies[0]->getActorId());
    //     static::assertSame($note->getId(), $replies[0]->getReplyTo());

    //     static::assertSame($user->getNickname(), $replies[0]->getReplyToNickname());
    // }

    public function testIsVisibleTo()
    {
        static::bootKernel();
        $actor                = DB::findOneBy(Actor::class, ['nickname' => 'taken_user']);
        $private_group        = DB::findOneBy(Actor::class, ['nickname' => 'taken_private_group']);
        $private_group_member = DB::findOneBy(Actor::class, ['nickname' => 'some_user']);
        $public_group         = DB::findOneBy(Actor::class, ['nickname' => 'taken_public_group']);

        $note_visible_to_1 = DB::findBy(Note::class, ['actor_id' => $actor->getId(), 'content' => 'private note', 'scope' => VisibilityScope::MESSAGE->value], limit: 1)[0];
        static::assertTrue($note_visible_to_1->isVisibleTo($actor));
        static::assertFalse($note_visible_to_1->isVisibleTo($private_group));
        static::assertFalse($note_visible_to_1->isVisibleTo($private_group_member));

        $note_public = DB::findBy(Note::class, ['actor_id' => $actor->getId(), 'content' => 'some other content'], limit: 1)[0];
        static::assertTrue($note_public->isVisibleTo($actor));
        static::assertTrue($note_public->isVisibleTo($private_group));
        static::assertTrue($note_public->isVisibleTo($private_group_member));

        $group_note = DB::findBy(Note::class, ['actor_id' => $actor->getId(), 'content' => 'group note private', 'scope' => VisibilityScope::GROUP->value], limit: 1)[0];
        static::assertTrue($group_note->isVisibleTo($private_group_member, in: $private_group));
        static::assertFalse($group_note->isVisibleTo($actor, in: $private_group));
        static::assertFalse($group_note->isVisibleTo($private_group, in: $private_group));
    }
}
