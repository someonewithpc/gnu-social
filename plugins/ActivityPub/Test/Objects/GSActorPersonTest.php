<?php

declare(strict_types = 1);

// {{{ License

// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.

// }}}

namespace Plugin\ActivityPub\Test\Objects;

use App\Entity\Actor;
use App\Util\GNUsocialTestCase;
use Plugin\ActivityPub\Entity\ActivitypubActor;
use Plugin\ActivityPub\Entity\ActivitypubRsa;
use Plugin\ActivityPub\Util\Explorer;

class GSActorPersonTest extends GNUsocialTestCase
{
    public function testPersonFromJson()
    {
        self::bootKernel();

        $person    = Explorer::getOneFromUri('https://instance.gnusocial.test/actor/42', try_online: false);
        $ap_person = ActivitypubActor::getByPK(['actor_id' => $person->getId()]);
        static::assertSame('https://instance.gnusocial.test/actor/42/inbox.json', $ap_person->getInboxUri());
        static::assertSame('https://instance.gnusocial.test/inbox.json', $ap_person->getInboxSharedUri());
        $person = $ap_person->getActor();
        static::assertSame('https://instance.gnusocial.test/actor/42', $person->getUri());
        static::assertSame(Actor::PERSON, $person->getType());
        static::assertSame('diogo', $person->getNickname());
        static::assertSame('Diogo Peralta Cordeiro', $person->getFullname());
        $public_key = ActivityPubRsa::getByActor($person)->getPublicKey();
        static::assertSame("-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArBB+3ldwA2qC1hQTtIho\n9KYhvvMlPdydn8dA6OlyIQ3Jy57ADt2e144jDSY5RQ3esmzWm2QqsI8rAsZsAraO\nl2+855y7Fw35WH4GBc7PJ6MLAEvMk1YWeS/rttXaDzh2i4n/AXkMuxDjS1IBqw2w\nn0qTz2sdGcBJ+mop6AB9Qt2lseBc5IW040jSnfLEDDIaYgoc5m2yRsjGKItOh3BG\njGHDb6JB9FySToSMGIt0/tE5k06wfvAxtkxX5dfGeKtciBpC2MGT169iyMIOM8DN\nFhSl8mowtV1NJQ7nN692USrmNvSJjqe9ugPCDPPvwQ5A6A61Qrgpz5pav/o5Sz69\nzQIDAQAB\n-----END PUBLIC KEY-----\n", $public_key);
    }
}
