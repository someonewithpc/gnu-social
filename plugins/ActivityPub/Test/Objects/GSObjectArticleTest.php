<?php

declare(strict_types = 1);

// {{{ License

// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.

// }}}

namespace Plugin\ActivityPub\Test\Objects;

use App\Core\VisibilityScope;
use App\Entity\Note;
use App\Util\GNUsocialTestCase;
use Plugin\ActivityPub\ActivityPub;
use Plugin\ActivityPub\Entity\ActivitypubObject;
use Plugin\ActivityPub\Util\Explorer;

class GSObjectArticleTest extends GNUsocialTestCase
{
    public function testNoteFromJson()
    {
        self::bootKernel();

        $actor_uri  = 'https://instance.gnusocial.test/actor/42';
        $object_uri = 'https://instance.gnusocial.test/object/note/1338';
        $group_uri  = 'https://instance.gnusocial.test/actor/21';
        $article    = ActivityPub::getObjectByUri($object_uri, try_online: false);
        static::assertInstanceOf(Note::class, $article);

        static::assertSame(Explorer::getOneFromUri($actor_uri)->getId(), $article->getActorId());
        static::assertSame('text/markdown', $article->getContentType());
        static::assertSame('This is an interesting article.', $article->getContent());
        static::assertSame('<p>This is an interesting article.</p>', $article->getRendered());
        static::assertSame('ActivityPub', $article->getSource());
        static::assertNull($article->getReplyTo());
        static::assertFalse($article->getIsLocal());
        static::assertSame(VisibilityScope::EVERYWHERE, $article->getScope());
        static::assertSame($object_uri, $article->getUrl());
        static::assertNull($article->getLanguageLocale());
        static::assertSame('article', $article->getType());
        static::assertSame('hello, world.', $article->getTitle());

        $ap_object = ActivitypubObject::getByPK(['object_uri' => $object_uri]);
        static::assertSame(Note::schemaName(), $ap_object->getObjectType());
        static::assertSame($object_uri, $ap_object->getObjectUri());
        static::assertSame($article->getId(), $ap_object->getObjectId());

        static::assertCount(1, $attT = $article->getAttentionTargets());
        static::assertObjectEquals(Explorer::getOneFromUri($group_uri, try_online: false), $attT[0]);
        static::assertSame([], $article->getMentionTargets());
    }
}
