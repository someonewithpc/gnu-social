<?php

declare(strict_types = 1);

// {{{ License
// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.
// }}}

/**
 * ActivityPub implementation for GNU social
 *
 * @package   GNUsocial
 * @category  ActivityPub
 *
 * @author    Diogo Peralta Cordeiro <@diogo.site>
 * @copyright 2018-2019, 2021 Free Software Foundation, Inc http://www.fsf.org
 * @license   https://www.gnu.org/licenses/agpl.html GNU AGPL v3 or later
 */

namespace Plugin\ActivityPub\Controller;

use ActivityPhp\Type\AbstractObject;
use App\Core\Controller;
use App\Core\DB;
use function App\Core\I18n\_m;
use App\Core\Log;
use App\Core\Queue;
use App\Core\Router;
use App\Entity\Actor;
use App\Util\Common;
use App\Util\Exception\ClientException;
use Exception;
use Plugin\ActivityPub\Entity\ActivitypubActor;
use Plugin\ActivityPub\Entity\ActivitypubRsa;
use Plugin\ActivityPub\Util\Explorer;
use Plugin\ActivityPub\Util\HTTPSignature;
use Plugin\ActivityPub\Util\Model;
use Plugin\ActivityPub\Util\TypeResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * ActivityPub Inbox Handler
 *
 * @copyright 2018-2019, 2021 Free Software Foundation, Inc http://www.fsf.org
 * @license   https://www.gnu.org/licenses/agpl.html GNU AGPL v3 or later
 */
class Inbox extends Controller
{
    public function onGet(Request $request, ?int $gsactor_id = null): TypeResponse
    {
        return new TypeResponse(json_encode(['error' => 'No AP C2S inbox yet.']), 400);
    }

    /**
     * Create an Inbox Handler to receive something from someone.
     */
    public function onPost(Request $request, ?int $gsactor_id = null): TypeResponse
    {
        $error = function (string $m, ?Exception $e = null): TypeResponse {
            Log::error('ActivityPub Error Answer: ' . ($json = json_encode(['error' => $m, 'exception' => var_export($e, true)])));
            if (\is_null($e)) {
                return new TypeResponse($json, 400);
            } else {
                throw $e;
            }
        };
        $path = Router::url('activitypub_inbox', type: Router::ABSOLUTE_PATH);

        if (!\is_null($gsactor_id)) {
            try {
                $user = DB::findOneBy('local_user', ['id' => $gsactor_id]);
                $path = Router::url('activitypub_actor_inbox', ['gsactor_id' => $user->getId()], type: Router::ABSOLUTE_PATH);
            } catch (Exception $e) {
                throw new ClientException(_m('No such actor.'), 404, previous: $e);
            }
        }

        Log::debug('ActivityPub Inbox: Received a POST request.');
        $body = (string) $this->request->getContent();
        Log::debug('ActivityPub Inbox: Request Body content: ' . $body);
        $type = Model::jsonToType($body);

        if ($type->has('actor') === false) {
            return $error('Actor not found in the request.');
        }

        $to_actor = $this->deriveActivityTo($type);

        try {
            $resource_parts = parse_url($type->get('actor'));
            if ($resource_parts['host'] !== Common::config('site', 'server')) {
                $actor = DB::wrapInTransaction(fn () => Explorer::getOneFromUri($type->get('actor'), try_online: true, on_behalf_of: $to_actor));
                $ap_actor = DB::findOneBy(ActivitypubActor::class, ['actor_id' => $actor->getId()]);
            } else {
                throw new Exception('Only remote actors can use this endpoint.');
            }
            unset($resource_parts);
        } catch (Exception $e) {
            return $error('Invalid actor.', $e);
        }

        $activitypub_rsa  = ActivitypubRsa::getByActor($actor);
        $actor_public_key = $activitypub_rsa->getPublicKey();

        $headers = $this->request->headers->all();
        Log::debug('ActivityPub Inbox: Request Headers.', [$headers]);
        // Flattify headers
        foreach ($headers as $key => $val) {
            $headers[$key] = $val[0];
        }

        if (!isset($headers['signature'])) {
            Log::debug('ActivityPub Inbox: HTTP Signature: Missing Signature header.');
            return $error('Missing Signature header.');
            // TODO: support other methods beyond HTTP Signatures
        }

        // Extract the signature properties
        $signatureData = HTTPSignature::parseSignatureHeader($headers['signature']);
        Log::debug('ActivityPub Inbox: HTTP Signature Data.', [$signatureData]);
        if (isset($signatureData['error'])) {
            return $error(json_encode($signatureData, \JSON_PRETTY_PRINT));
        }

        [$verified, /*$headers*/] = HTTPSignature::verify($actor_public_key, $signatureData, $headers, $path, $body);

        // If the signature fails verification the first time, update profile as it might have changed public key
        if ($verified !== 1) {
            try {
                $res = Explorer::getRemoteActorActivity($ap_actor->getUri(), $to_actor);
                if (\is_null($res)) {
                    return $error('Invalid remote actor (null response).');
                }
            } catch (Exception $e) {
                return $error('Invalid remote actor.', $e);
            }
            try {
                ActivitypubActor::update_profile($ap_actor, $actor, $activitypub_rsa, $res);
            } catch (Exception $e) {
                return $error('Failed to update remote actor information.', $e);
            }

            [$verified, /*$headers*/] = HTTPSignature::verify($actor_public_key, $signatureData, $headers, $path, $body);
        }

        // If it still failed despite profile update
        if ($verified !== 1) {
            Log::debug('ActivityPub Inbox: HTTP Signature: Invalid signature.');
            return $error('Invalid signature.');
        }

        // HTTP signature checked out, make sure the "actor" of the activity matches that of the signature
        Log::debug('ActivityPub Inbox: HTTP Signature: Authorised request. Will now start the inbox handler.');

        Queue::enqueue(
            payload: [$ap_actor, $actor, $type],
            queue: 'ActivitypubInbox',
            priority: false,
        );

        return new TypeResponse($type, status: 202);
    }

    /**
     * Poke at the given AbstractObject to find out who it is 'to'.
     * Function will check through the 'to', 'cc', and 'object' fields
     * of the given type (in that order) to check if if points to anyone
     * on our instance. The first found candidate will be returned.
     *
     * @param AbstractObject $type
     *
     * @return Actor|null The discovered actor, if found. null otherwise.
     *
     * @throws Exception
     */
    private function deriveActivityTo(AbstractObject $type): Actor|null
    {
        foreach (['to', 'cc'] as $field) {
            foreach ((array) $type->get($field) as $uri) {
                $actor = self::uriToMaybeLocalActor($uri);
                if (!\is_null($actor)) {
                    return $actor;
                }
            }
        }

        // if it's not to or cc anyone we have to dive deeper
        if ($type->has('object')) {
            // the 'object' field might just be a uri of one
            // of our Actors, if this is a follow or block
            $object = $type->get('object');
            if (\is_string($object)) {
                $actor = self::uriToMaybeLocalActor($object);
                if (!\is_null($actor)) {
                    return $actor;
                }
            } else if ($object instanceof AbstractObject) {
                // if the inner object is also a Type, repeat the process
                return $this->deriveActivityTo($object);
            }
        }

        return null;
    }

    /**
     * Get local actor that owns or corresponds to given uri.
     *
     * @param string $uri
     *
     * @return Actor|null
     */
    private static function uriToMaybeLocalActor(string $uri): Actor|null
    {
        $parsed = parse_url($uri);
        // check if this uri belongs to us
        if ($parsed['host'] === Common::config('site', 'server')) {
            // it is our uri so we should be able to get
            // the actor without making any remote calls
            $actor = Explorer::getLocalActorForPath($parsed['path']);
            if (!\is_null($actor)) {
                return $actor;
            }
        }
        return null;
    }
}
