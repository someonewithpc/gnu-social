<?php

declare(strict_types = 1);

// {{{ License
// This file is part of GNU social - https://www.gnu.org/software/social
//
// GNU social is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GNU social is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with GNU social.  If not, see <http://www.gnu.org/licenses/>.
// }}}

/**
 * ActivityPub implementation for GNU social
 *
 * @package   GNUsocial
 * @category  Actor
 *
 * @author    Diogo Peralta Cordeiro <@diogo.site>
 * @copyright 2022 Free Software Foundation, Inc http://www.fsf.org
 * @license   https://www.gnu.org/licenses/agpl.html GNU AGPL v3 or later
 */

namespace Plugin\UnboundGroup;

use App\Core\DB;
use App\Core\Event;
use App\Core\Modules\Plugin;
use App\Entity\Actor;
use EventResult;
use Plugin\ActivityPub\Entity\ActivitypubActor;
use Plugin\UnboundGroup\Controller\GroupSettings;
use Plugin\UnboundGroup\Entity\activitypubGroupUnbound;
use Symfony\Component\HttpFoundation\Request;

/**
 * When enabled, Adds ActivityPub "FEP-2100 Unbound Group and Organization" support to GNU social
 *
 * @copyright 2022 Free Software Foundation, Inc http://www.fsf.org
 * @license   https://www.gnu.org/licenses/agpl.html GNU AGPL v3 or later
 */
class UnboundGroup extends Plugin
{
    /**
     * @param SettingsTabsType $tabs
     */
    public function onPopulateSettingsTabs(Request $request, string $section, array &$tabs): EventResult
    {
        if ($section === 'profile' && $request->get('_route') === 'group_actor_settings') {
            $tabs[] = [
                'title'      => 'Unbound',
                'desc'       => 'Unbound Group state.',
                'id'         => 'settings-group-unbound-details',
                'controller' => GroupSettings::groupUnbound($request, Actor::getById((int) $request->get('id')), 'settings-group-unbound-details'),
            ];
            $tabs[] = [
                'title'      => 'Linked',
                'desc'       => 'Link to this Group from another Group.',
                'id'         => 'settings-group-links-details',
                'controller' => GroupSettings::groupLinks($request, Actor::getById((int) $request->get('id')), 'settings-group-links-details'),
            ];
        }
        return Event::next;
    }

    public function onActivityStreamsTwoContext(array &$activity_streams_two_context): EventResult
    {
        $activity_streams_two_context[] = [
            'unbound' => [
                '@id'   => 'gs:unbound',
                '@type' => '@id',
            ],
        ];
        return Event::next;
    }

    public function onActivityPubAddActivityStreamsTwoData(string $type_name, object &$type): EventResult
    {
        if ($type_name === 'Group' || $type_name === 'Organization') {
            $actor   = \Plugin\ActivityPub\Util\Explorer::getOneFromUri($type->getId());
            $unbound = DB::findOneBy(activitypubGroupUnbound::class, ['actor_id' => $actor->getId()], return_null: true);

            if (!\is_null($unbound_value = $unbound?->getUnbound())) {
                $type->set('unbound', $unbound_value);
            }
        }
        return Event::next;
    }

    public function onActivityPubCreateOrUpdateActor(\ActivityPhp\Type\AbstractObject $object, Actor $actor, ActivitypubActor $ap_actor): EventResult
    {
        if ($object->has('unbound')) {
            $obj        = DB::findOneBy(activitypubGroupUnbound::class, ['actor_id' => $actor->getId()], return_null: true);
            $update_obj = activitypubGroupUnbound::createOrUpdate(obj: $obj, args: ['actor_id' => $actor->getId(), 'unbound' => $object->get('unbound')]);
            if (\is_null($obj)) {
                DB::persist($update_obj);
            }
            DB::flush();
        }
        return Event::next;
    }
}
